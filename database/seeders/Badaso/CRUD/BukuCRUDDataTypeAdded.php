<?php

namespace Database\Seeders\Badaso\CRUD;

use Illuminate\Database\Seeder;
use Uasoft\Badaso\Facades\Badaso;
use Uasoft\Badaso\Models\MenuItem;

class BukuCRUDDataTypeAdded extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     *
     * @throws Exception
     */
    public function run()
    {
        \DB::beginTransaction();

        try {

            $data_type = Badaso::model('DataType')->where('name', 'buku')->first();

            if ($data_type) {
                Badaso::model('DataType')->where('name', 'buku')->delete();
            }

            \DB::table('badaso_data_types')->insert(array (
                'id' => 1,
                'name' => 'buku',
                'slug' => 'buku',
                'display_name_singular' => 'Buku',
                'display_name_plural' => 'Buku',
                'icon' => 'book',
                'model_name' => NULL,
                'policy_name' => NULL,
                'controller' => NULL,
                'order_column' => NULL,
                'order_display_column' => NULL,
                'order_direction' => NULL,
                'generate_permissions' => true,
                'server_side' => false,
                'is_maintenance' => 0,
                'description' => NULL,
                'details' => NULL,
                'notification' => '[]',
                'is_soft_delete' => false,
                'created_at' => '2024-02-03T03:19:06.000000Z',
                'updated_at' => '2024-02-03T05:28:27.000000Z',
            ));

            Badaso::model('Permission')->generateFor('buku');

            $menu = Badaso::model('Menu')->where('key', config('badaso.default_menu'))->firstOrFail();

            $menu_item = Badaso::model('MenuItem')
                ->where('menu_id', $menu->id)
                ->where('url', '/general/buku')
                ->first();

            $order = Badaso::model('MenuItem')->highestOrderMenuItem($menu->id);

            if (!is_null($menu_item)) {
                $menu_item->fill([
                    'title' => 'Buku',
                    'target' => '_self',
                    'icon_class' => 'book',
                    'color' => null,
                    'parent_id' => null,
                    'permissions' => 'browse_buku',
                    'order' => $order,
                ])->save();
            } else {
                $menu_item = new MenuItem();
                $menu_item->menu_id = $menu->id;
                $menu_item->url = '/general/buku';
                $menu_item->title = 'Buku';
                $menu_item->target = '_self';
                $menu_item->icon_class = 'book';
                $menu_item->color = null;
                $menu_item->parent_id = null;
                $menu_item->permissions = 'browse_buku';
                $menu_item->order = $order;
                $menu_item->save();
            }

            \DB::commit();
        } catch(Exception $e) {
            \DB::rollBack();

           throw new Exception('Exception occur ' . $e);
        }
    }
}
