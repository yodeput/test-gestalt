<?php

namespace Database\Seeders\Badaso\CRUD;

use Illuminate\Database\Seeder;
use Uasoft\Badaso\Traits\Seedable;

class BadasoDeploymentOrchestratorSeeder extends Seeder
{
    use Seedable;

    protected $seedersPath = 'database/seeders/Badaso/CRUD/';

    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        $this->seed(BukuCRUDDataTypeAdded::class);
        $this->seed(BukuCRUDDataRowAdded::class);
        $this->seed(RentCRUDDataTypeAdded::class);
        $this->seed(RentCRUDDataRowAdded::class);
        $this->seed(CustomerCRUDDataTypeAdded::class);
        $this->seed(CustomerCRUDDataRowAdded::class);
    }
}
